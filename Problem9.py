def pythagorean_triplet(n):
	product = 0
	for a in range(n):
		for b in range(a,n):
			c = n - a - b
			if a**2 + b**2 == c**2:
				product = a*b*c
	return product
