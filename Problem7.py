from Problem3 import isprime

def nPrime(n):
	result = 0
	number = 1
	test = 0
	while number <= n:
		if isprime(test):
			result = test
			number += 1
			test += 1
		else:
			test += 1 
	return result
