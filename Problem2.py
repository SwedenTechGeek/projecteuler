def fibonaccisum(highest_number):
    genlst = []
    fibsum = 0
    tmp = 0
    test = 0
    count = 0
    while test <= highest_number:
        if len(genlst) <= 2:
            genlst.append(count)
            test = count
            count = count + 1
        else:
            tmp = genlst[count-1] + genlst[count-2]
            genlst.append(tmp)
            test = tmp
            count = count + 1
    for i in genlst:
        if i%2==0:
            fibsum = fibsum + i
    return fibsum
