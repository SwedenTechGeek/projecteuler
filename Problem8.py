def adjacentN(lst, n):
	product = 0
	for i in range(len(lst)):
		tmp = lst[i]
		for j in range(n-1):
			if (i+j+1) < len(lst):
				tmp = tmp * lst[i+j+1]
		if product < tmp:
			product = tmp
	return product
	
