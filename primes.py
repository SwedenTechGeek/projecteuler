class calcPrimes():
	def __init__(self, x):
		self.a = x

	def isprime(self):
		if int(self.a) <= 1:
			return False
		elif int(self.a) <= 3:
			return True
		elif int(self.a)%2 == 0 or int(self.a)%3 == 0:
			return False
		i = 5
		while i * i <= int(self.a):
			if int(self.a)%i == 0 or int(self.a)%(i+2) == 0:
				return False
			i = i + 6
		return True
