# ProjectEuler
My solutions to the project Euler problems I solved so far.
This is something I do in my sparetime when I have the time to do it.

#run.py
To be used to trigger and input test data in the solutions.

#ProblemX.py
The different solutions I implemented.

#Other .py files
This is other python files that sometimes are broken out and used in several different problems. For example primes.py
