import math

def primefactor(n):
	i = 2
	while i * i < n:
     		while n % i == 0:
         		n = n / i
     		i = i + 1

	return n

def isprime(a):
	if a <= 1:
		return False
	elif a <= 3:
		return True
	elif a%2 == 0 or a%3 == 0:
		return False
	i = 5
	while i * i <= a:
		if a%i == 0 or a%(i+2) == 0:
			return False
		i = i + 6
	return True


