def isPalNum(n):
	lsttmp = []
	revtmp = []
	tmp = ""
	for i in str(n):
		lsttmp.append(int(i))
	for i in range(len(lsttmp)):
		tmp += str(lsttmp[len(lsttmp)-1-i])
	if int(tmp) == n:
		return True
	else:
		return False

def findpal(n):
	if n == 2:
		value = 99
	elif n == 3:
		value = 999
	highest = [0,0,0]
	while value > 0:
		for i in range(value):
			if isPalNum(value*i):
				if (value*i) > highest[0]:
					highest[0] = value*i
					highest[1] = value
					highest[2] = i
		value = value - 1
	return highest
