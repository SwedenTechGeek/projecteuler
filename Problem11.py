def gridprod(di, n):
	product = 0
	tmp = 1
	for i in range(len(di)):
		for j in range(len(di[i])):
			if (j>2 and i>2) and (j <= 16 and i <= 16): #main
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up left
					tmp = tmp * di[i-x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up right
					tmp = tmp * di[i-x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				
				for x in range(n): #dia down right
					tmp = tmp * di[i+x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down left
					tmp = tmp * di[i+x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j<3 and i<3): #upper left corner
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down right
					tmp = tmp * di[i+x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j<3 and (i>2 and i<=16)): #left wall
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up right
					tmp = tmp * di[i-x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down right
					tmp = tmp * di[i+x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j<3 and i > 16): #lower left corner
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up right
					tmp = tmp * di[i-x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j>2 and j<=16) and i<3: # roof
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down right
					tmp = tmp * di[i+x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down left
					tmp = tmp * di[i+x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j>2 and j<=16) and i>16: #Floor
				for x in range(n): #direction right
					tmp = tmp * di[i][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up left
					tmp = tmp * di[i-x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up right
					tmp = tmp * di[i-x][j+x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif (j>16 and i<3): # Upper right corner
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down left
					tmp = tmp * di[i+x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif j>16 and (i>2 and i<=16): # right wall
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction down
					tmp = tmp * di[i+x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up left
					tmp = tmp * di[i-x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia down left
					tmp = tmp * di[i+x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
			elif j>16 and i>16: #lower right corner
				for x in range(n): #direction up
					tmp = tmp * di[i-x][j]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #direction left
					tmp = tmp * di[i][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
				for x in range(n): #dia up left
					tmp = tmp * di[i-x][j-x]
				if product < tmp:
					product = tmp
				tmp = 1
	return product
