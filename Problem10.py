from Problem3 import isprime

def sumPrime(n):
	summa = 0
	tmp = []
	count = 0
	test = 1
	while count < n:
		if isprime(test):
			tmp.append(test)
			count += 1
			test += 1
		else:
			count += 1
			test += 1
	for i in tmp:
		summa += i
	return summa
