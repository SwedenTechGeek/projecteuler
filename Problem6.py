def sumSquareDiff(n):
	result = 0
	sumsquare = 0
	squaresum = 0
	for i in range(n):
		sumsquare = sumsquare + ((i+1)**2)
	for j in range(n):
		squaresum = squaresum + (j+1)
	squaresum = squaresum**2
	result = squaresum - sumsquare
	#print sumsquare
	#print squaresum
	return result

